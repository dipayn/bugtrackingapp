﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//For the Sql Server, we have to import
using System.Data;
using System.Data.SqlClient;



namespace BugTrackingApp
{
    class Connection
    {
        SqlConnection cn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Temp\MyDatabase.mdf;Integrated Security=True;Connect Timeout=30");

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataTable dt = new DataTable();
        //For insert update and delete statement
        public void manipulate(String query)

        {
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public DataTable retrieve(string query)
        {
            DataSet ds = new DataSet();
            da = new SqlDataAdapter(query,cn);
            da.Fill(ds);
            return ds.Tables[0];

        }

    }
}
