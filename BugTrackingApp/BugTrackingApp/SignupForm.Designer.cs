﻿namespace BugTrackingApp
{
    partial class SignupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FirstnameTxtBox = new System.Windows.Forms.TextBox();
            this.LastnameTxtBox = new System.Windows.Forms.TextBox();
            this.UsernameTxtBox = new System.Windows.Forms.TextBox();
            this.PasswordTxtBox = new System.Windows.Forms.TextBox();
            this.SignupBtn = new System.Windows.Forms.Button();
            this.JobTxtBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LoginLinkBtn = new System.Windows.Forms.Button();
            this.SignupExitBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please Enter Your First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Please Enter Your Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Please Enter A Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Please Enter A Password";
            // 
            // FirstnameTxtBox
            // 
            this.FirstnameTxtBox.Location = new System.Drawing.Point(234, 22);
            this.FirstnameTxtBox.Name = "FirstnameTxtBox";
            this.FirstnameTxtBox.Size = new System.Drawing.Size(160, 20);
            this.FirstnameTxtBox.TabIndex = 4;
            // 
            // LastnameTxtBox
            // 
            this.LastnameTxtBox.Location = new System.Drawing.Point(234, 59);
            this.LastnameTxtBox.Name = "LastnameTxtBox";
            this.LastnameTxtBox.Size = new System.Drawing.Size(160, 20);
            this.LastnameTxtBox.TabIndex = 5;
            // 
            // UsernameTxtBox
            // 
            this.UsernameTxtBox.Location = new System.Drawing.Point(234, 101);
            this.UsernameTxtBox.Name = "UsernameTxtBox";
            this.UsernameTxtBox.Size = new System.Drawing.Size(160, 20);
            this.UsernameTxtBox.TabIndex = 6;
            // 
            // PasswordTxtBox
            // 
            this.PasswordTxtBox.Location = new System.Drawing.Point(234, 142);
            this.PasswordTxtBox.Name = "PasswordTxtBox";
            this.PasswordTxtBox.Size = new System.Drawing.Size(160, 20);
            this.PasswordTxtBox.TabIndex = 7;
            this.PasswordTxtBox.TextChanged += new System.EventHandler(this.PasswordTxtBox_TextChanged);
            // 
            // SignupBtn
            // 
            this.SignupBtn.Location = new System.Drawing.Point(163, 269);
            this.SignupBtn.Name = "SignupBtn";
            this.SignupBtn.Size = new System.Drawing.Size(75, 23);
            this.SignupBtn.TabIndex = 8;
            this.SignupBtn.Text = "Sign Up";
            this.SignupBtn.UseVisualStyleBackColor = true;
            this.SignupBtn.Click += new System.EventHandler(this.SignupBtn_Click);
            // 
            // JobTxtBox
            // 
            this.JobTxtBox.FormattingEnabled = true;
            this.JobTxtBox.Items.AddRange(new object[] {
            "admin",
            "debugger",
            "tester"});
            this.JobTxtBox.Location = new System.Drawing.Point(234, 193);
            this.JobTxtBox.Name = "JobTxtBox";
            this.JobTxtBox.Size = new System.Drawing.Size(121, 21);
            this.JobTxtBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Please Your Job";
            // 
            // LoginLinkBtn
            // 
            this.LoginLinkBtn.Location = new System.Drawing.Point(31, 318);
            this.LoginLinkBtn.Name = "LoginLinkBtn";
            this.LoginLinkBtn.Size = new System.Drawing.Size(102, 23);
            this.LoginLinkBtn.TabIndex = 11;
            this.LoginLinkBtn.Text = "Back To Login";
            this.LoginLinkBtn.UseVisualStyleBackColor = true;
            this.LoginLinkBtn.Click += new System.EventHandler(this.LoginLinkBtn_Click);
            // 
            // SignupExitBtn
            // 
            this.SignupExitBtn.Location = new System.Drawing.Point(357, 317);
            this.SignupExitBtn.Name = "SignupExitBtn";
            this.SignupExitBtn.Size = new System.Drawing.Size(75, 23);
            this.SignupExitBtn.TabIndex = 12;
            this.SignupExitBtn.Text = "Exit";
            this.SignupExitBtn.UseVisualStyleBackColor = true;
            this.SignupExitBtn.Click += new System.EventHandler(this.SignupExitBtn_Click);
            // 
            // SignupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 353);
            this.Controls.Add(this.SignupExitBtn);
            this.Controls.Add(this.LoginLinkBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.JobTxtBox);
            this.Controls.Add(this.SignupBtn);
            this.Controls.Add(this.PasswordTxtBox);
            this.Controls.Add(this.UsernameTxtBox);
            this.Controls.Add(this.LastnameTxtBox);
            this.Controls.Add(this.FirstnameTxtBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SignupForm";
            this.Text = "SignupForm";
            this.Load += new System.EventHandler(this.SignupForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox FirstnameTxtBox;
        private System.Windows.Forms.TextBox LastnameTxtBox;
        private System.Windows.Forms.TextBox UsernameTxtBox;
        private System.Windows.Forms.TextBox PasswordTxtBox;
        private System.Windows.Forms.Button SignupBtn;
        private System.Windows.Forms.ComboBox JobTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button LoginLinkBtn;
        private System.Windows.Forms.Button SignupExitBtn;
    }
}