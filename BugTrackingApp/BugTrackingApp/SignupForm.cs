﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugTrackingApp
{
    public partial class SignupForm : Form
    {
        public SignupForm()
        {
            InitializeComponent();
        }

        private void PasswordTxtBox_TextChanged(object sender, EventArgs e)
        {

        }
        // we are adding the connection link to this 
        Connection con = new Connection();

        private void SignupBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FirstnameTxtBox.Text) || string.IsNullOrEmpty(LastnameTxtBox.Text) || string.IsNullOrEmpty(UsernameTxtBox.Text) || string.IsNullOrEmpty(PasswordTxtBox.Text) || string.IsNullOrEmpty(JobTxtBox.Text))
            {
                MessageBox.Show("Fill all the missing fields!!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FirstnameTxtBox.Focus();
            }
            else
            {

                String query;


                query = "Insert into user_table(firstname,lastname,username,pass,user_type_id) values('" + FirstnameTxtBox.Text + "', '" + LastnameTxtBox.Text + "', '" + UsernameTxtBox.Text + "', '" + PasswordTxtBox.Text + "'," + Convert.ToInt32(JobTxtBox.SelectedValue) + ")";
                con.manipulate(query);

            }
                    


                }

       
          
        

        private void LoginLinkBtn_Click(object sender, EventArgs e)
        {
            LoginForm frm = new LoginForm();
            this.Hide();
            frm.Show();

        }

        private void SignupExitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SignupForm_Load(object sender, EventArgs e)
        {
            string query;
            string temp;
            temp = FirstnameTxtBox.Text;
            query = "Select * from usertype ";
            DataTable dta = con.retrieve(query);

            JobTxtBox.DataSource = dta;
            JobTxtBox.DisplayMember = "user_type";
            JobTxtBox.ValueMember = "user_type_id";
        }
    }
}
