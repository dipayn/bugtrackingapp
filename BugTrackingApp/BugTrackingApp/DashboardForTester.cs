﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTrackingApp
{
    public partial class DashboardForTester : Form
    {
        public DashboardForTester()
        {
            InitializeComponent();
        }

        private void BugReportBtn_Click(object sender, EventArgs e)
         String uname;
        Connection conn = new Connection();
        public Tester(string value)
        {
            InitializeComponent();
            uname = value;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string query;


            query = "insert into bug_details(project_name,method,class,line_num,bug_name,error,comments,status,username,fixed_by) values('" + txtprojectname.Text + "','" + txtmethod.Text + "','" + txtclass.Text + "'," + txtline.Text + ",'" + txtbugname.Text + "','" + txterror.Text + " ','" + txtcomment.Text + "','Not Fixed','" + uname + "','Not yet')";
            conn.manipulate(query);
            postbutton.Text = "POST";
            display();

        }
        public void display()
        {
            string query = "select * from bug_details order by bug_id desc";
            DataTable dt = conn.retrieve(query);
            dataGridView1.DataSource = dt;
        }

        private void Tester_Load(object sender, EventArgs e)
        {
            display();
        }
    }
}
